from lib2to3 import fixer_base
from lib2to3.fixer_util import touch_import
from lib2to3.pgen2 import token


class FixBasestring(fixer_base.BaseFix):
    BM_compatible = True
    PATTERN = "'basestring'"

    def transform(self, node, results):
        if node.type == token.NAME:
            touch_import(None, 'six', node)
            new = node.clone()
            new.value = 'six.string_types'

            return new
