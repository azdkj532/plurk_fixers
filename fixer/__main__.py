#!/usr/bin/env python2.7
import sys
from lib2to3.main import main

sys.path.append('.')
sys.exit(main('fixer', sys.argv[1:]))
