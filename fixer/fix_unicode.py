r"""Fixer for unicode.

* Changes unicode to str and unichr to chr.

* If "...\u..." is not unicode literal change it into "...\\u...".

* Change u"..." into "...".

"""

from lib2to3 import fixer_base
from lib2to3.fixer_util import touch_import
from lib2to3.pgen2 import token

_mapping = {"unichr": "chr", "unicode": "text_type"}


class FixUnicode(fixer_base.BaseFix):
    BM_compatible = True
    PATTERN = "STRING | 'unicode' | 'unichr'"

    def start_tree(self, tree, filename):
        super(FixUnicode, self).start_tree(tree, filename)
        self.unicode_literals = 'unicode_literals' in tree.future_features

    def transform(self, node, results):
        if node.type == token.NAME:
            new = node.clone()
            new.value = _mapping[node.value]
            if node.value == 'unichr':
                touch_import('six.moves.builtins', 'chr', node)
            elif node.value == 'unicode':
                touch_import('six', 'text_type', node)

            return new
