# Copyright 2007 Google, Inc. All Rights Reserved.
# Licensed to PSF under a Contributor Agreement.

"""Fixer for dict methods.

from six import itervalues
d.keys() -> list(d)
d.items() -> list(iteritems(d))
d.values() -> list(itervalues(d))

from six import iteritems, itervalues
d.iterkeys() -> d  # for key in d: ...
d.iteritems() -> iteritems(d)
d.itervalues() -> itervalues(d)

d.viewkeys() -> d.keys()
d.viewitems() -> d.items()
d.viewvalues() -> d.values()

Except in certain very specific contexts: the iter() can be dropped
when the context is list(), sorted(), iter() or for...in; the list()
can be dropped when the context is list() or sorted() (but not iter()
or for...in!). Special contexts that apply to both: list(), sorted(), tuple()
set(), any(), all(), sum().

Note: iter(d.keys()) could be written as iter(d) but since the
original d.iterkeys() was also redundant we don't fix this.  And there
are (rare) contexts where it makes a difference (e.g. when passing it
as an argument to a function that introspects the argument).
"""

# Local imports
from lib2to3 import pytree, patcomp, fixer_base, fixer_util, pygram
from lib2to3.fixer_util import Node, Name, Call, Dot, Leaf, FromImport, find_root, touch_import


iter_exempt = fixer_util.consuming_calls | {"iter"}


class FixDict(fixer_base.BaseFix):
    BM_compatible = True

    PATTERN = """
    power< head=any+
         trailer< '.' method=('keys'|'items'|'values'|
                              'iterkeys'|'iteritems'|'itervalues'|
                              'viewkeys'|'viewitems'|'viewvalues') >
         parens=trailer< '(' ')' >
         tail=any*
    >
    """

    def transform(self, node, results):
        head = results["head"]
        method = results["method"][0] # Extract node for method name
        tail = results["tail"]
        syms = self.syms
        method_name = method.value
        isiter = method_name.startswith("iter")
        isview = method_name.startswith("view")
        if isiter or isview:
            assert method_name[4:] in ("keys", "items", "values"), repr(method)
        else:
            assert method_name in ("keys", "items", "values"), repr(method)

        head = [n.clone() for n in head]
        tail = [n.clone() for n in tail]
        if isview:
            args = head + [
                pytree.Node(syms.trailer, [
                    Dot(), Name(method_name[4:], prefix=method.prefix)
                ]), results["parens"].clone()
            ]
            new = pytree.Node(syms.power, args)
            new.prefix = ""
        else:
            new = pytree.Node(syms.power, head)
            new.prefix = ""
            if method_name.endswith('values'):
                new = Call(Name('itervalues'), [new])
                touch_import('six', Name('itervalues'), results['node'])
            if method_name.endswith('items'):
                new = Call(Name('iteritems'), [new])
                touch_import('six', Name('iteritems'), results['node'])

            if not isiter:
                new = Call(Name('list'), [new])

        if tail:
            new = pytree.Node(syms.power, [new] + tail)
        new.prefix = node.prefix
        return new
