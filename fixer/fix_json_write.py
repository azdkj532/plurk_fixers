from lib2to3.fixer_base import BaseFix
from lib2to3.pygram import python_symbols


class FixJsonWrite(BaseFix):

    PATTERN = """
              power< 'json'
                  trailer< '.' func='write' > trailer< '(' args=any ')' >
              >
    """

    def transform(self, node, result):
        args = result['args']
        if args.type == python_symbols.arglist:
            if args.children[-1].children[0].value == 'js_date':
                args.children.pop()
                args.children.pop()
                args.changed()
        result['func'].value = 'dumps'
        result['func'].changed()
