import itertools
from lib2to3 import fixer_base
from lib2to3.fixer_util import Call
from lib2to3.fixer_util import KeywordArg
from lib2to3.fixer_util import Name
from lib2to3.fixer_util import touch_import
from lib2to3.pgen2 import token
from lib2to3.pygram import python_symbols as symbols


class FixSorted(fixer_base.BaseFix):
    PATTERN = """
              power< "sorted" trailer< "(" ARGLIST=arglist< any+ > ")" > any* >
    """

    def transform_keyword(self, nodes, result):
        '''transform second and later position argument into keyword argument'''
        arglist = itertools.takewhile(
            lambda a: a.type != symbols.argument, [arg for arg in nodes if arg.type != token.COMMA]
        )
        template = [None, 'cmp', 'key', 'reverse']
        for arg, key in zip(arglist, template):
            if not key:
                continue
            arg_ = arg.clone()
            arg_.prefix = ''
            new_arg = KeywordArg(Name(key), arg_)
            new_arg.prefix = arg.prefix
            arg.replace(new_arg)

    def transform_cmp(self, nodes, result):
        '''transform argument `cmp` into `key`'''
        cmp_node = None
        key_node = None
        for arg in nodes:
            if arg.type == symbols.argument:
                if arg.children[0].value == 'cmp':
                    cmp_node = arg
                if arg.children[0].value == 'key':
                    key_node = arg

        if cmp_node:
            if key_node:
                return  # Do nothing when it have both cmp and key argument

            cmp_node.children[0].value = 'key'
            cmp_node.children[2].replace(
                Call(Name('cmp_to_key'), args=[cmp_node.children[2].clone()])
            )
            touch_import('functools', 'cmp_to_key', cmp_node)

    def transform(self, node, result):
        self.transform_keyword(result['ARGLIST'].children, result)
        self.transform_cmp(result['ARGLIST'].children, result)
