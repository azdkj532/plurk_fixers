# Copyright 2006 Google, Inc. All Rights Reserved.
# Licensed to PSF under a Contributor Agreement.

"""Fixer that turns 'long' into 'int' everywhere.
"""

# Local imports
from lib2to3 import fixer_base
from lib2to3.fixer_util import is_probably_builtin, touch_import, Name


class FixInteger(fixer_base.BaseFix):
    BM_compatible = True
    PATTERN = "'long'"

    def transform(self, node, results):
        if is_probably_builtin(node):
            node.value = "int"
            node.changed()
            touch_import('six.moves.builtins', Name('int'), results['node'])
