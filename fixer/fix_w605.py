import re
import itertools
from lib2to3 import fixer_base
from lib2to3.fixer_util import Call, String, Name, touch_import


class FixW605(fixer_base.BaseFix):
    BM_compatible = True
    PATTERN = "STRING+"

    def transform(self, node, results):
        flag = ''.join(itertools.takewhile(lambda x: x not in '\'\"', node.value))
        string = ''.join(itertools.dropwhile(lambda x: x not in '\'\"', node.value))
        u = 'u' in flag
        r = 'r' in flag

        if re.search(r'\\[<_/BdDsSwW\-\|\?\!\.\(\)\[\]\*\+]', string):
            r = True

        if u and r:
            touch_import(None, 'six', node)
            return Call(Name('six.u'), [String('r' + string)], prefix=node.prefix)

        elif r:
            return String('r' + string, prefix=node.prefix)
