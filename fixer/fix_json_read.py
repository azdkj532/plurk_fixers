from lib2to3.fixer_base import BaseFix
from lib2to3.pgen2 import token
from lib2to3.main import main


class FixJsonRead(BaseFix):
    """fix json.read"""

    # order = "post"  # "pre": get leaf before get the node
    # _accept_type = token.NAME

    PATTERN = """
              power< first='json'
                  trailer< '.' func='read' > trailer< '(' [any] ')' > >
    """

    def transform(self, node, result):
        func = result['func']
        func.value = 'loads'
        func.changed()
