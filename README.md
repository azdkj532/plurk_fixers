# Plurk Fixer

## Usage

```bash
$ python fixer/main.py -f json_read <filename>
```

Check manual page for detail.
```bash
man 2to3
```


## Fixers

### json_read

From `json.read(...)` to `json.loads(...)`

### json_write
From `json.write(data, js_date=False)` to  `json.dumps(data)`.
This fixer will remove argument `js_date`.

### urllib
Fix urllib module to `six.moves` solution.

Note that this fixer will create some duplicate import statement.
For example,
```python
import urllib
import urllib2
```
will be transform into 
```
from six.moves import urllib_parse, urllib_request
from six.moves import urllib_parse, urllib_request
```
NOTE: run built-in imports before this.

### dict
Use six solution as above.
In `plurk.web.settings`, this generates duplicated import statement.
